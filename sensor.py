from switch import BlueSwitch
from pin_definitions import Pins
from machine import Pin, ADC
from biorobotics import AnalogIn
from filter import Emg_processing


class SensorState(object):
    """ Reads sensors and updates specific variables """
    def __init__(self):
        self.blue_switch = BlueSwitch()
        self.switch_value = 0
        self.pins = Pins()
        self.filter_emg1 = Emg_processing()
        self.filter_emg2 = Emg_processing()
        self.filter_emg3 = Emg_processing()
        self.filter_emg4 = Emg_processing()
        return

    def update(self):
        self.switch_value = self.blue_switch.value()
        emg_1 = AnalogIn(self.pins.EMG_1).read()        # R bicep
        self.emg1_filtered = self.filter_emg1.filter_signal(emg_1)
        self.emg_2 = AnalogIn(self.pins.EMG_2).read()        # L bicep
        self.emg_3 = AnalogIn(self.pins.EMG_3).read()        # R calf
        self.emg_4 = AnalogIn(self.pins.EMG_4).read()        # L calf
        self.button_1 = ADC(self.pins.BUTTON_1).read_u16()  # Open
        self.button_2 = ADC(self.pins.BUTTON_2).read_u16()  # Close
        self.button_3 = ADC(self.pins.BUTTON_3).read_u16()  # Tilt
        ### UPDATE CURRENT ANGLES ###
        return