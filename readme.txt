files:
 - main.py              initiates robot by running state_machine.py
 - state_machine.py     connects different files and classes. can start and stop the state_machine
 - state_functions.py   contains all states
 - states.py            changes current state
 - switch.py            Changes switch_value when blue button on microcontroller is pressed
 - sensor.py            reads sensors and updates specific variables
 - pin_definitions.py   defines all used pins for (stepper)motors, buttons and EMG-input
 - motor.py             selects motor(s) and gives direction/PWM
 - leds.py              defines all leds