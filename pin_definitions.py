class Pins(object):
    """ Defines all used pins for (stepper)motors, buttons and EMG-input """

    MOTOR_1_DIRECTION = 'D4'
    MOTOR_1_PWM = 'D5'
    MOTOR_2_DIRECTION = 'D7' 
    MOTOR_2_PWM = 'D6'
    EMG_1 = 'A0'
    EMG_2 = 'A1'
    EMG_3 = 'A2'
    EMG_4 = 'A3'
    SERVO_1 = 'D10'  # draaier
    SERVO_2 = 'D9'   # grijper
    BUTTON_1 = 'A4'  # open
    BUTTON_2 = 'A5'  # close
    BUTTON_3 = 'D12' # draaier