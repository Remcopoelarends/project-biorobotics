from biorobotics import Ticker
from states import State
from sensor import SensorState
from state_functions import StateFunctions
from filter import Emg_processing

class StateMachine(object):
    """ Connects different files and classes. can start and stop the state_machine """
    def __init__(self, ticker_frequency):
        self.robot_state = State()
        self.sensor_state = SensorState()
        self.state_functions = StateFunctions(self.robot_state, self.sensor_state, ticker_frequency)
        self.ticker = Ticker(0, ticker_frequency, self.run)
        return

    def run(self):
        self.sensor_state.update()
        self.state_functions.callbacks[self.robot_state.current]()
        return

    def start(self):
        self.ticker.start()
        return

    def stop(self):
        self.ticker.stop()
        return
