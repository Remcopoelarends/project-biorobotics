from states import State
from sensor import SensorState
from leds import Leds
from motor import Motor
from biorobotics import SerialPC, PWM
from pin_definitions import Pins
import utime
from calibration import Calibration
from kinematics import Kinematics

class StateFunctions(object):
    """ Contains all states, with entry, action and state guard """
    def __init__(self, robot_state, sensor_state, ticker_frequency):
        self.ticker_frequency = ticker_frequency
        self.robot_state = robot_state
        self.sensor_state = sensor_state
        self.leds = Leds()
        self.pc = SerialPC(5)
        self.active_emg = 0
        self.calibration_finished = 0
        self.servo1 = PWM(Pins.SERVO_1, 200)
        self.servo2 = PWM(Pins.SERVO_2, 200)
        self.servo1_value = 0.1
        self.servo2_value = 0.1
        self.button_threshold = 50000
        self.previous_servo2_value = 0.1
        self.step = 0.0005
        self.servo_max = 0.52
        self.servo_min = self.step + 0.1
        self.servo1_max = 0.22
        self.emg_1_threshold = 1000000
        self.emg_2_threshold = 1000000        
        self.emg_3_threshold = 1000000
        self.emg_4_threshold = 1000000
        self.threshold_factor = 8
        self.emg_calibration = Calibration(self.sensor_state)
        self.kinematics = Kinematics(self.ticker_frequency)
        self.motor = Motor(self.ticker_frequency)


        self.callbacks = {
            State.SAFE: self.safe,
            State.READ: self.read,
            State.MOVE: self.move,
            State.EQUILIBRIUM: self.equilibrium,
            State.CALIBRATION: self.calibration,
            State.POSITION: self.position,
            State.OPEN: self.open,
            State.TILT_FORWARD: self.tilt_forward,
            State.CLOSE: self.close
            }
        return

    def safe(self):

        # ENTRY
        if self.robot_state.is_changed():
            self.robot_state.set(State.SAFE)
            print('Entry: safe state')
        
        # ACTION
            # Move to equilibrium-position
        print('SAFE')
        self.servo1.write(self.servo1_value)
        self.servo2.write(self.servo2_value)

        # STATE GUARD
            # if equilibrium reached, change to equilibrium-state
        if self.sensor_state.switch_value == 1:
            self.robot_state.set(State.CALIBRATION)
            #self.robot_state.set(State.MOVE)
        return

    def equilibrium(self):

        # ENTRY
        if self.robot_state.is_changed():
            # Keep position
            # Turn on LED (green?) (maybe external?)
            pass


        # ACTION
        print('EQUILIBRIUM')
            # measure EMG-signals

        # STATE GUARD
        if self.calibration_finished:
            self.robot_state.set(State.POSITION)
        else:
            self.robot_state.set(State.CALIBRATION)
        return

    def calibration(self):

        # ENTRY
        if self.robot_state.is_changed():
            self.robot_state.set(State.CALIBRATION)
            
            # Measure EMG for specific time and calculate threshold for that specific EMG-connection
            # Maybe make a sound/use another LED to tell the user to stop contract muscle
            # Save measured connection in list
        
        # ACTION
        print('CALIBRATION')
        self.averages = self.emg_calibration.emg_sum()
            # measure EMG-signals
            # if EMG measured (>= starting_threshold) -> do the same steps like the entry state
            # Measure EMG for specific time and calculate threshold for that specific EMG-connection
            # Maybe make a sound/use another LED to tell the user to stop contract muscle
            # Save measured connection in list

        # STATE GUARD
        if self.sensor_state.switch_value == 1 or self.averages[0] != 0:
            self.emg_thresholds = [element*self.threshold_factor for element in self.averages]
            self.emg_gain = [8,1,1,1]
            self.robot_state.set(State.POSITION)
        return

    def position(self):
        # ENTRY
        if self.robot_state.is_changed():
            self.robot_state.set(State.POSITION)
        
        # ACTION
        print('POSITION')
        ### keep position ###
        
        # STATE GUARD
            # if button 1 is pushed: -> open state
        if self.sensor_state.button_1 > self.button_threshold+100000:
            self.robot_state.set(State.OPEN)
            
            # if button 2 is pushed: -> close state
        elif self.sensor_state.button_2 > self.button_threshold+100000:
            self.robot_state.set(State.CLOSE)
            
            # if button 3 is pushed: -> tilt state
        elif self.sensor_state.button_3 > self.button_threshold+100000:
            self.robot_state.set(State.TILT_FORWARD)            
            
            # if EMG_1 is sensed:      -> move state        
        elif self.sensor_state.emg1_filtered > self.emg_thresholds[0]:
            self.active_emg = 1
            self.robot_state.set(State.MOVE)
            
            # if EMG_2 is sensed:      -> move state        
        elif self.sensor_state.emg_2 > self.emg_thresholds[1]+100000:
            self.active_emg = 2
            self.robot_state.set(State.MOVE)
            
            # if EMG_3 is sensed:      -> move state      
        elif self.sensor_state.emg_3 > self.emg_thresholds[2]+100000:
            self.active_emg = 3
            self.robot_state.set(State.MOVE)
            
            # if EMG_4 is sensed:      -> move state        
        elif self.sensor_state.emg_4 > self.emg_thresholds[3]+100000:
            self.active_emg = 4
            self.robot_state.set(State.MOVE)
        
        return

    def open(self):
        # ENTRY
        if self.robot_state.is_changed():
            self.robot_state.set(State.OPEN)

        # ACTION
        print('OPEN')
        print(self.servo1_value)
        
        # write signal to servo
        if self.servo1_value < self.servo1_max - self.step:
            self.servo1_value += self.step
            self.servo1.write(self.servo1_value)
        
        # STATE GUARD
        if self.sensor_state.button_1 < self.button_threshold:
            self.robot_state.set(State.POSITION)
        return

    def tilt_forward(self):
        
        # ENTRY
        if self.robot_state.is_changed():
            self.robot_state.set(State.TILT_FORWARD)
            self.previous_servo2_value = self.servo2_value
        
        print('TILT')
        print(self.servo2_value)
        # ACTION
            
        ##### lift end-effector a little bit #####

        # write signal to servo
        if self.servo2_value < self.servo_max - self.step:
            self.servo2_value += self.step
            self.servo2.write(self.servo2_value)
                 
        # STATE GUARD
        if self.sensor_state.button_3 < self.button_threshold:
            self.servo2.write(self.previous_servo2_value)
            self.servo2_value = self.previous_servo2_value
            self.robot_state.set(State.POSITION)
        return

    def close(self):
        # ENTRY
        if self.robot_state.is_changed():
            self.robot_state.set(State.CLOSE)

        # ACTION
        print('CLOSE')
        print(self.servo1_value)
        
        # write signal to servo
        if self.servo1_value > self.servo_min + self.step:
            self.servo1_value -= self.step
            self.servo1.write(self.servo1_value)
        
        
        # STATE GUARD
        if self.sensor_state.button_2 < self.button_threshold:
            self.robot_state.set(State.POSITION)
        return

    def move(self):

        # ENTRY
        if self.robot_state.is_changed():
            self.robot_state.set(State.MOVE)
                
        # ACTION
        print('MOVE')
        # Movement according to measured emg-signal
        if self.active_emg == 1:
            emg1 = self.sensor_state.emg1_filtered
            q = self.kinematics.calc_q([0,1])
            motor1_input = emg1*q[0][0]*self.emg_gain[0]
            motor2_input = emg1*q[1][0]*self.emg_gain[0]
            self.motor.write_motor(motor1_input, motor2_input)
            self.kinematics.calc_current_angles()
            print(motor1_input)
            print(motor2_input)
            #omega = self.kinematics.calc_ee_rotation(self, motor1_input, motor2_input)
            #self.servo2.write(omega)

            # STATE GUARD
            if emg1 < self.emg_thresholds[0]:
                self.robot_state.set(State.POSITION)
                self.motor.write_motor(0,0)
        
        """
        elif self.active_emg == 2:
            emg2 = self.filter_emg2.filter_signal(self.sensor_state.emg_2)    
            #q = self.kinematics.calc_q([-1,0])
            #self.motor.write_motor(emg2*q[1]*self.emg_gain[1], emg2*q[1]*self.emg_gain[1])        
            
            # STATE GUARD
            if emg2 < self.emg_thresholds[1]:
                self.robot_state.set(State.POSITION)
        
        elif self.active_emg == 3:
            emg3 = self.filter_emg3.filter_signal(self.sensor_state.emg_3)    
            #q = self.kinematics.calc_q([0,1])
            #self.motor.write_motor(emg3*q[0]*self.emg_gain[2], emg3*q[1]*self.emg_gain[2])
            
            # STATE GUARD
            if emg3 < self.emg_thresholds[2]:
                self.robot_state.set(State.POSITION)        
        
        elif self.active_emg == 4:
            emg4 = self.filter_emg4.filter_signal(self.sensor_state.emg_4)    
            #q = self.kinematics.calc_q([0,-1])
            #self.motor.write_motor(emg4*q[0]*self.emg_gain[3], emg4*q[1]*self.emg_gain[3])
            
            # STATE GUARD
            if emg4 < self.emg_thresholds[3]:
                self.robot_state.set(State.POSITION)
        """
        return



    def read(self):
        pass
        return