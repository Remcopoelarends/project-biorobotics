from pyb import LED

class Leds(object):
    """ Defines all leds """
    def __init__(self):
        self.green = LED(1)
        self.yellow = LED(2)
        self.red = LED(3)
