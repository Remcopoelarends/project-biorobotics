from pyb import LED

class State(object):
    """ Contains all states, can set current_state and can measure if state has changed """
    SAFE = 'safe'
    READ = 'read'
    MOVE = 'move'
    READ = 'read'
    EQUILIBRIUM = 'equilibrium'
    CALIBRATION = 'calibration'
    POSITION = 'position'
    OPEN = 'open'
    TILT = 'tilt'
    CLOSE = 'close'
    TILT_FORWARD = 'tilt_forward'
    WAIT = 'wait'


    def __init__(self):
        self.previous = None
        self.current = self.SAFE
        return

    def set(self,state):
        self.previous = self.current
        self.current = state
        return

    def is_changed(self):
        return self.previous is not self.current


    