from filter import Biquad


class PID(object):
    
    def __init__(self, frequency):
        self.biquad = Biquad()
        
        self.time_step = 1/frequency
        self.P_gain = 1
        self.I_gain = 0.5
        self.D_gain = 0
        self.error_prev = 0
        self.accumulated_error = 0
        
        return
    
    def step(self, sample_ref_pot, sample_measured_encoder, my_filter = 1):
        sample_measured_pot = -sample_measured_encoder / 8400
        self.error = sample_ref_pot - sample_measured_pot
        print(sample_measured_pot)
        
        # Proportional
        P = self.error
        
        # Integration
        self.accumulated_error += self.error * self.time_step
        
        # Differentiation
        if my_filter:
            self.error_change = self.biquad.step((self.error - self.error_prev)) / self.time_step
        self.error_change = (self.error - self.error_prev) / self.time_step
         
        self.output_PWM = (P * self.P_gain) + (self.accumulated_error * self.I_gain) + (self.error_change * self.D_gain)
        
        self.error_prev = self.error
        print(self.error)

        return self.output_PWM
    