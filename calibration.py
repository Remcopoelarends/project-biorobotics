from filter import Emg_processing
from biorobotics import SerialPC

class Calibration(object):
    
    def __init__(self, sensorstate):
        self.emg_1_sum = 0
        self.emg_2_sum = 0
        self.emg_3_sum = 0
        self.emg_4_sum = 0
        self.counter = 0
        self.sensorstate = sensorstate

        return
        
    def emg_sum(self):
        emg_1 = self.sensorstate.emg1_filtered
        emg_2 = 0
        emg_3 = 0
        emg_4 = 0
        #self.emg_1 = self.filter_emg1.filter_signal(self.sensorstate.emg_1)
        #self.emg_2 = self.filter_emg2.filter_signal(self.sensorstate.emg_2)
        #self.emg_3 = self.filter_emg3.filter_signal(self.sensorstate.emg_3)
        #self.emg_4 = self.filter_emg4.filter_signal(self.sensorstate.emg_4)
        self.emg_1_sum += emg_1
        self.emg_2_sum += emg_2
        self.emg_3_sum += emg_3
        self.emg_4_sum += emg_4
        self.counter += 1
        
        if self.counter > 200:
            return self.emg_average()
        return [0, 0, 0, 0]
        
    def emg_average(self):
        self.average_emg_1 = self.emg_1_sum / self.counter
        self.average_emg_2 = self.emg_2_sum / self.counter
        self.average_emg_3 = self.emg_3_sum / self.counter
        self.average_emg_4 = self.emg_4_sum / self.counter
        averages = [self.average_emg_1, self.average_emg_2, self.average_emg_3, self.average_emg_4]
        return averages