import numpy as np
from scipy.linalg import expm
import math

L1 = 0.35
L2 = 0.3
T1 = np.array([1,0,0])
T2 = np.array([1,0,-L1])
Hee0 = np.eye(3)
Hee0[:2,2] = (L1,-L2)
q_current = [np.pi/8,0]
dt = 1/200
k = 1

def tilde(twist):
    matrix = np.array([[0,-1,twist[1]],
                        [1,0,twist[2]],
                        [0,0,0]])      
    return matrix

def calcH():
    # calculate H_ee_to_0 for q (angles)
    expm1 = expm(tilde(T1)*q_current[0])
    expm2 = expm(tilde(T2)*q_current[1])
    H = expm1 @ expm2 @ Hee0        
    return H

H = calcH()
print(H)
print(math.acos(H[0][0])*180/np.pi)