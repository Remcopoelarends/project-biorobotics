import ulab as np
import math

class Kinematics(object):
    
    def __init__(self, frequency):
        self.L1 = 0.35
        self.L2 = 0.3
        self.T1 = np.array([1,0,0])
        self.T2 = np.array([1,0,-self.L1])
        self.Hee0 = np.eye(3)
        self.Hee0[:2,2] = np.array([[self.L1],[-self.L2]])
        q_ref_conf = np.array([90,90])
        self.q_current = q_ref_conf
        self.dt = 1/frequency
        self.k = 1
        
        return

    def calcH(self):
        # calculate H_ee_to_0 for q (angles)
        expm1 = self.expm(self.q_current[0], -self.T1[2], self.T1[1])
        expm2 = self.expm(self.q_current[1], -self.T1[2], self.T1[1])
        expm1_2 = np.linalg.dot(expm1, expm2)
        H = np.linalg.dot(expm1_2, self.Hee0)        
        return H

    def calcJ(self):
        # calculate Jacobian
        J = np.zeros((3,2))
        J[:,0] = np.array([[1], [0], [0]])
        J[:,1] = np.array([[1], [self.L1*math.cos(self.q_current[0])], [self.L1*math.sin(self.q_current[1])]])
        return J

    def calc_q(self, ps_derivative_des):    
        ps_derivative_des = np.array([[ps_derivative_des[0]], [ps_derivative_des[1]]]) 
        # calculate H_ee_to_0 and Jacobian
        Hee0 = self.calcH()
        J = self.calcJ()    

        # create new frame f in ee
        Hf0 = np.eye(3)
        Hf0[:2,2] = Hee0[:2,2]

        # express J in frame f
        AdH0f = self.Adjoint(self.inverseH(Hf0))
        J_prime = np.linalg.dot(AdH0f, J)
        
        # remove rotational velocities from J_prime
        J_double_prime = J_prime[1:,:]
        
        # calculate needed joint velocities
        self.j_velocity = np.linalg.dot(np.linalg.inv(J_double_prime), ps_derivative_des)
        
        if abs(self.j_velocity[0][0]) > abs(self.j_velocity[1][0]):
            max_velocity = abs(self.j_velocity[0][0])
        else:
            max_velocity = abs(self.j_velocity[1][0])
        
        
        self.j_velocity_norm = self.j_velocity / max_velocity
        
        return self.j_velocity_norm
    
    def get_des_velocity_ee(self,x,y):
        ps_derivative = [x,y]
        return ps_derivative   
    
    def calc_current_angles(self):
        # self.q_current[0] = self.q_current[0] + sample_measured_encoder_1/8400
        # self.q_current[1] = self.q_current[1] + sample_measured_encoder_2/8400
        
        ########################################

        self.q_current[0] = self.q_current[0] + self.j_velocity_norm[0]*self.dt*self.k
        self.q_current[1] = self.q_current[1] + self.j_velocity_norm[1]*self.dt*self.k
        return
    
    def calc_ee_rotation(self, joint_vel1, joint_vel2):
        J = self.calcJ()
        joint_vel = np.array([joint_vel1], [joint_vel2])
        omega = np.linalg.dot(J, joint_vel)[0]
        return omega


    def expm(self, theta, rx, ry):
        m1 = np.array([[1,0,rx],
                        [0,1,ry],
                        [0,0,1]])
        m2 = np.array([[math.cos(theta), -math.sin(theta), 0],
                        [math.sin(theta), math.cos(theta), 0],
                        [0, 0, 1]])
        
        m3 = np.array([[1,0,-rx],
                        [0,1,-ry],
                        [0,0,1]])
        
        m4 = np.linalg.dot(m1, m2)
        expm_matrix = np.linalg.dot(m4,m3)
        
        return  expm_matrix

    def tilde(self, twist):
        matrix = np.array([[0,-1,twist[1]],
                            [1,0,twist[2]],
                            [0,0,0]])      
        return matrix
    
    def inverseH(self, H_matrix):
        H_inv = self.my_copy(H_matrix)
        H_inv[0:2,0:2] = H_matrix[0:2,0:2].transpose()
        H_inv[0:2,2] = np.linalg.dot(-1*H_matrix[0:2,0:2].transpose(), H_matrix[0:2,2])
        H_inv[2,0:3] = np.array([0,0,1])
        return H_inv

    def Adjoint(self, H_matrix):
            H_adjoint = self.my_copy(H_matrix)
            H_adjoint[1:3,1:3] = H_matrix[0:2,0:2]
            H_adjoint[2,0] = -1*H_matrix[0,2]
            H_adjoint[1,0] = H_matrix[1,2]
            H_adjoint[0,0:3] = np.array([1, 0, 0])
            return H_adjoint
        
    def my_copy(self, matrix):
        copy_matrix = np.zeros((len(matrix),len(matrix)))
        for i in range(len(matrix)):
            for j in range(len(matrix)):
                copy_matrix[i,j] = matrix[i,j]
                
        return copy_matrix
    
    

