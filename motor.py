from pin_definitions import Pins
from biorobotics import PWM
from machine import Pin

class Motor(object):
    """ Selects motor(s) and gives direction/PWM """
    def __init__(self, frequency):
            self.pwm1 = PWM(Pins.MOTOR_1_PWM, frequency)
            self.direction1 = Pin(Pins.MOTOR_1_DIRECTION, Pin.OUT)
            self.pwm2 = PWM(Pins.MOTOR_2_PWM, frequency)
            self.direction2 = Pin(Pins.MOTOR_2_DIRECTION, Pin.OUT)
            return
        
    def write_motor(self,pwm_value_1, pwm_value_2):
        if pwm_value_1 < 0:
            self.direction1.value(1)
        else: 
            self.direction1.value(0)
        if abs(pwm_value_1) > 1:
            pwm_value_1 = 1    
        self.pwm1.write(abs(pwm_value_1))
        
        if pwm_value_2 < 0:
            self.direction2.value(1)
        else: 
            self.direction2.value(0)
        if abs(pwm_value_2) > 1:
            pwm_value_2 = 1    
        self.pwm2.write(abs(pwm_value_2))        
        
        return