from biorobotics import AnalogIn, SerialPC, Ticker

class Emg_processing:
    def __init__(self):
        self.w1 = 0.0
        self.w2 = 0.0
        self.y1 = 0.0
        self.y2 = 0.0
        self.y3 = 0.0
        self.y4 = 0.0
        self.x1 = 0.0
        self.x2 = 0.0
        self.x3 = 0.0
        self.x4 = 0.0
        print('init klaar')

    def low_pass(self, x):
        # 3 Hz
        b0 = 0.002080567135492
        b1 = 0.004161134270985
        b2 = 0.002080567135492
        a1 = -1.866892279711715
        a2 = 0.875214548253684
        
        y = float(b0*x + self.w1)
        self.w1 = float(b1*x - a1*y + self.w2)
        self.w2 = float(b2*x - a2*y)
        return y

    def high_pass(self, x):
        # 2 Hz
        b0 = 0.432846644990292
        b1 = -1.73138657996117
        b2 = 2.59707986994175
        b3 = -1.73138657996117
        b4 = 0.432846644990292
        a0 = 1
        a1 = -2.36951300718204
        a2 = 2.31398841441588
        a3 = -1.05466540587857
        a4 = 0.187379492368185

        y = (1/a0)*(x*b0 + self.x1*b1 + self.x2*b2 + self.x3*b3 + self.x4*b4) - (self.y1*a1 + self.y2*a2 + self.y3*a3 + self.y4*a4)
        self.y4 = self.y3
        self.y3 = self.y2
        self.y2 = self.y1
        self.y1 = y       
        
        self.x4 = self.x3
        self.x3 = self.x2
        self.x2 = self.x1
        self.x1 = x
        
        return y

    def filter_signal(self, emg_signal):
        
        y_high_passed = self.high_pass(emg_signal)
        y_abs = abs(y_high_passed)
        y_low_passed = self.low_pass(y_abs)
        return y_low_passed
"""
processing = Emg_processing()
emg = AnalogIn('A0')
pc = SerialPC(2)
def loop():
    pc.set(0, emg.read())
    pc.set(1, processing.filter_signal(emg.read()))
    pc.send()
    
    
ticker = Ticker(0, 250, loop, enable_gc=True)
ticker.start()
"""